<!-- What is node ? -->

Node is a platform for running primarly, though not exclusively, server-side code that has high performance and is capable of handling large request loads with ease.
In node everything is non blocking, meaning code wont hold up processing other requests threads.
Node is single threaded first it feel like bottle neck but in fact its a net benefit because its avoid context switching.
Node is event driven and single threaded with background workers.

Intercat with local file system, access relational databses, call remote systems and much more.

<!-- NPM -->

npm install express
npm is cli program that is npm itself and install is one command and express is an argument in it.

<!-- Semantic Versioning -->

"1.2.3" - Grab specific version
"~1.2.3" - grab most recent patch version 1.2.x
"^1.2.3" - grab most recent patch version 1.x.x
"\*" - It will grab newest version

<!-- Doubts -->

When you fire off some types of I/O request , Node will generally spawn new thread for that. but while its doing it work that single event-driven thread continues executing your code. All of this is managed by event queue mecahanism so that the call backs for those I/O operatiosn are fired, back on the single thread when the responses come back. All this mean is there is no context switch between threads but also that teh single thread is never sitting idle .

Difference in WebServer and AppServer?
Write a webserver - what does this mean?
What is local web server?
What is call back function here ?
What is webserver instance?
Method return a refernce to webserver instance-- ??

