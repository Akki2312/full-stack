//http is a module this is one of the core modules that nodes comes with out of the box and as such it compiled directly in node binary.
//The require() function returns an object that is essentially the API provided by the module.
//This return object  can include have methods, attribute or whatever you want.It could be some var with  data in array
//Here one of the method return by  object is createserver() . This method create a web server instance and returns refernce to it.
//The argument passed in this method is a function that serves as request listner that is the function can be executed any time a request made to teh server.
//Function handels all incoming HTTP request
// Interrogate the incoming request to determine the http method.
// Parse the request path.
// Examine the header value.
//CreateServer method returns a refernce to the web server instance , which itself contains the method listen()
//This method accepts a port number on which server should listen or optionally the hostname /IP address on which to listen.
//Standard HTTP port 80 is sepcified and by default the local machine loopback adddress 127.0.0.1 is used if no IP address is specified as is the case here
//Once you call this method the server will begin listening for each request that comes in, it will call the anonynoms function passed create server.
//

require("http")
  .createServer((inRequest, inResponse) => {
    inResponse.end("Hello from my first server");
  })
  .listen(80);
console.log(require("http"));
